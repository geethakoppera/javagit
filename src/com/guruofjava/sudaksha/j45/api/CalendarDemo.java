package com.guruofjava.sudaksha.j45.api;

import java.util.Calendar;
import java.util.Date;

public class CalendarDemo {
	public static void main(String[] args) {
		Calendar c1=Calendar.getInstance();
		System.out.println(c1);
		System.out.println(c1.get(Calendar.YEAR));
		System.out.println(c1.get(Calendar.DAY_OF_WEEK));
		System.out.println(c1.get(Calendar.DAY_OF_MONTH));
		System.out.println(c1.get(Calendar.DAY_OF_YEAR));
		System.out.println(c1.get(Calendar.MONTH));
		//c1.add(Calendar.DAY_OF_YEAR, 156);
		
		//c1.add(Calendar.MONTH,50);
		//c1.set(Calendar.MONTH,50);
		//c1.set(Calendar.DAY_OF_YEAR, 31);
		c1.set(Calendar.MONTH,8);
		//c1.add(Calendar.DAY_OF_YEAR, 31);
		Date d1=c1.getTime();
		System.out.println(c1.getTime());
	}

}
