package com.guruofjava.sudaksha.j45.inheritance;

public class Cat extends Animal {

	public void move(int a) {

		System.out.println("cat is travelling " + a + " distance");
	}

	public   void whoAmI() {
		System.out.println("cat");

	}

	public void hunt() {
		System.out.println("cat is hunting");
	}
}
