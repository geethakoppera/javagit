package com.guruofjava.sudaksha.j45.recap;

public class BasicsDemo {
	public static void main(String[] args) {
		
	}
	public static void main4(String[] args) {
		int c = 67934;
		int i;
		int temp;
		temp = c;
		int even = 0;
		int odd = 0;
		while (temp > 0) {
			i = temp % 10;
			temp = temp / 10;
			/*if (i % 2 == 0) {
				even = even + i;
			}

			else {
				odd = odd + i;
			}*/
        i= (i%2==0)?(even=even+i):(odd= odd+i);
		}
		System.out.println(even + " " + odd);

	}

	public static void main3(String[] args) {
		int b = 54321;
		int result = 0;
		int temp;
		temp = b;
		while (temp > 0) {
			result = result + temp % 10;
			temp = temp / 10;
		}
		System.out.println(result);
	}

	public static void main2(String[] args) {
		int age = 16;
		if (age <= 15) {
			System.out.println("child policy can be used");
		} else {
			System.out.println("child policy cannot be issued");
		}
	}

	public static void main1(String[] args) {
		int i = 10;
		System.out.println(i);

		i = 010;
		System.out.println(i);

		i = 0x10;
		System.out.println(i);

		i = 0b1100;
		System.out.println(i);

		i = 543_21;
		System.out.println(i);

		i = 0b1010_1111;
		System.out.println(i);

	}

}
