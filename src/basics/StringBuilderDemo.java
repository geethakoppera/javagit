package basics;

public class StringBuilderDemo {
	public static void main(String[] args) {
		int i;

		StringBuilder sb = new StringBuilder("2010,2012,2020,2030,2040");

		// sb.append("james gosling" );

		/*
		 * System.out.println(sb); sb.append( 10); System.out.println(sb); sb.insert(13,
		 * 20); System.out.println(sb); sb.insert(13, " "); System.out.println(sb);
		 */

		while ((i = sb.indexOf("0")) != -1) {
			sb.replace(i, i + 1, "x");
		}
		System.out.println(sb);

		int count = 0;

		while ((i = sb.lastIndexOf("x")) != -1) {
			count++;

			sb.replace(i, i + 1, "X");
			if (count == 4) {
				break;
			}
			
		}
		System.out.println(sb);

	}
}
