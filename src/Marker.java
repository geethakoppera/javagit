
public class Marker {

	private double price;
	public final  String brand;
	String color;
	
	public Marker() {
		price=25.0;
		color="Black";
		brand="camlin";
		

		
	}
	public Marker(String b) {
		price=25.0;
		brand=b;
		color="Black";
	}
	public Marker(String b,String c)
	{
		price=25.0;
		brand=b;
		color=c;
	}
	double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price >= 0) {
			this.price = price;
		} else {
			System.out.println("Invalid price for marker");
		}
	}

	public void write(String v) {
		System.out.println(v);
	}

	public void write(int v) {
		System.out.println(v);
	}
}
