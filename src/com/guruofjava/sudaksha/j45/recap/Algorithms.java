package com.guruofjava.sudaksha.j45.recap;

public class Algorithms {

	public static int evenSum(int a) {
		int result=0,i;
		while(a>0) {
			i=a%10;
			a=a/10;
			result=result+(i%2==0?i:0);
		}
		
		return result;
		}
	public static int oddSum(int a) {
		int result=0,i;
		while(a>0) {
			i=a%10;
			a=a/10;
			result=result+(i%2!=0?i:0);
		}
		
		return result;
	}
	public static void sort(int[] d) {
		int temp;
		for(int i=0;i<d.length-1;i++) {
			for(int j=0;j<(d.length-1)-i;j++) {
				if(d[j]>d[j+1]) {
					temp=d[j];
					d[j]=d[j+1];
					d[j+1]=temp;
					
				}
			}
			
		}
		
	}
	public static void main(String[] args) {
		//System.out.println(evenSum(56412));
		//System.out.println(oddSum(56412));
		int[] a= {12,8,6,10,5,2};
		sort(a);
		for(int i=0;i<a.length;i++) {
			
			System.out.print(a[i]+ " ");
		}
	}
}
