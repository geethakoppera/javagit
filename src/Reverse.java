import java.util.Scanner;

public class Reverse {


	public int reverseNumber(int n) {
		int reverse = 0;
		while (n!= 0) {
			reverse = (reverse * 10) + (n% 10);
			n = n / 10;
		}
		return reverse;
	}

	public static void main(String a[]) {
		Reverse nr = new Reverse();
		
		System.out.println("Reveresed no." + nr.reverseNumber(15678));
	}
}


