package com.guruofjava.sudaksha.j45.inheritance;

public class InheritanceDemo {
	public static void main(String[] args) {
		Animal a1=new Animal();
		a1.whoAmI();
		Animal a2=new Cat();
		a2.whoAmI();
		Cat c1=new Cat();
		c1.whoAmI();
		
		
		/*a1.move(10);
		Cat c1=new  Cat();
		c1.move(78);
		Bird b1=new Bird();
		b1.move(97);*/
	}

}
